
.PHONY: build-ansible
build-ansible:
	docker build -t ansible-left ansible

.PHONY: left
left:
	docker run --rm -v "${CURDIR}/ansible:/ansible:ro" ansible ansible-playbook -i hosts.yml left.yml
